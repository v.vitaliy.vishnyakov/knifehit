﻿namespace _Core
{
    public interface IFallingObject
    {
        public void StartFalling();
        public void ClearParent();
    }
}