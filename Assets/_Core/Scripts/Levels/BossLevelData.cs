﻿namespace _Core.Scripts.Levels
{
    public class BossLevelData: LevelData
    {
        public int KnivesGoal { get; }
        public int LevelNumber { get; }
        public BossData BossData { get; }

        public BossLevelData(int knivesGoal, int levelNumber, BossData bossData)
        {
            KnivesGoal = knivesGoal;
            BossData = bossData;
            LevelNumber = levelNumber;
        }
    }
}