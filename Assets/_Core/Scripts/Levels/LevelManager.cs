﻿using System.Collections.Generic;
using _Core.Levels;
using _Core.Scripts.Levels;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Core
{
    public class LevelManager : Singleton<LevelManager>
    {
        [Header("Parameters")]
        [SerializeField] private LevelGenerationParameters _lvlGenParameters;
        [SerializeField] private float _appleSpawnOffset;
        [Header("Objects references")]
        [SerializeField] private Stump _stump;
        [SerializeField] private CrackedStump _crackedStump;
        [Header("Bosses Data")] 
        [SerializeField] private List<BossData> _commonBosses;
        [SerializeField] private List<BossData> _rareBosses;
        
        private float _stumpRadius;

        protected override void Initialize()
        {
            _stumpRadius = _stump.GetComponent<CircleCollider2D>().radius;
        }
        private void Start()
        {
            SubscribeToGameEvents();
        }
        private void SubscribeToGameEvents()
        {
            GameEventsModel gameEventsModel = new GameEventsModel
            {
                onLevelCompletion = OnLevelCompletionHandler,
                onKnifeStumpHit = _stump.OnKnifeHitHandler,
                onGameOver = OnGameOverHandler
            };
            GameManager.SubscribeToGameEvents(gameEventsModel);
        }

        public static LevelData SetupLevel(int levelNumber)
        {
            LevelData levelData;
            if (levelNumber % _instance._lvlGenParameters.bossFrequency == 0)
            {
                levelData = _instance.SetupBossLevel(levelNumber);
            }
            else
            {
                levelData = _instance.SetupCasualLevel(levelNumber);
            }
            _instance.SpawnKnivesOnStump();
            _instance.SpawnApple();
            _instance.ActivateStump();
            return levelData;
        }

        private BossLevelData SetupBossLevel(int levelNumber)
        {
            BossData bossData;
            if (Random.value <= _lvlGenParameters.rareBossChance)
            {
                bossData = GetRandomBoss(_rareBosses);
            }
            else
            {
                bossData = GetRandomBoss(_commonBosses);
            }
            _stump.Initialize(bossData.stumpConfiguration.Config);
            _stump.SetSkin(bossData.skin);
            return new BossLevelData(7, levelNumber, bossData);
        }

        private BossData GetRandomBoss(List<BossData> availableBosses)
        {
            return availableBosses[Random.Range(0, availableBosses.Count)];
        }

        private CasualLevelData SetupCasualLevel(int levelNumber)
        {
            StumpConfig stumpConfig = GenerateStumpConfigurationByLevel(levelNumber);
            _stump.Initialize(stumpConfig);
            _stump.SetDefaultSkin();
            int knifeGoal = GetRandomKnifeGoal();
            return new CasualLevelData(knifeGoal, levelNumber);
        }

        private int GetRandomKnifeGoal()
        {
            return Random.Range(_lvlGenParameters.minGoal, _lvlGenParameters.maxGoal);
        }

        private StumpConfig GenerateStumpConfigurationByLevel(int levelNumber)
        {
            int levelOnStage = levelNumber % _lvlGenParameters.bossFrequency;
            int stage = levelNumber / _lvlGenParameters.bossFrequency + 1;
            float modifier = 1 + stage / _lvlGenParameters.difficultyDivider;
            if (levelOnStage <= _lvlGenParameters.bossFrequency*0.5f)
            {
                ConstStumpConfig csc = (ConstStumpConfig) _lvlGenParameters.defaultConstStumpConfiguration.Config;
                return new ConstStumpConfig
                {
                    direction = Utils.GetRandomSign(),
                    speed = 360 / (csc.speed * modifier)
                };
            }

            if (levelOnStage <= _lvlGenParameters.bossFrequency * 0.75f)
            {
                DashStumpConfig dsc = (DashStumpConfig) _lvlGenParameters.defaultDashStumpConfiguration.Config;
                return new DashStumpConfig
                {
                    direction = Utils.GetRandomSign(),
                    angle = dsc.angle * modifier,
                    time = dsc.time / modifier
                };
            }

            RandomStumpConfig rsc = (RandomStumpConfig) _lvlGenParameters.defaultRandomStumpConfiguration.Config;
            return new RandomStumpConfig
            {
                minRotateAngle = rsc.minRotateAngle * modifier,
                maxRotateAngle = rsc.maxRotateAngle * modifier,
                minRotateTime = rsc.minRotateTime / modifier,
                maxRotateTime = rsc.maxRotateTime / modifier,
            };
        }

        private void SpawnApple()
        {
            if (Random.value > _instance._lvlGenParameters.appleSpawnChance) return;
            Apple apple = SpawnManager.SpawnApple();
            Transform appleTransform = apple.transform;
            _stump.AddStuckObject(apple.gameObject);
            appleTransform.localPosition = Random.insideUnitCircle.normalized * (_stumpRadius + _appleSpawnOffset);
            Utils.LookAt2D(appleTransform, _stump.transform.position);
        }

        private void SpawnKnivesOnStump()
        {
            int knivesNumber = Random.Range(0, _lvlGenParameters.maxKnivesOnStump + 1);
            for (int i = 0; i < knivesNumber; i++)
            {
                Knife knife = SpawnManager.SpawnKnife();
                Transform knifeTransform = knife.transform;
                _stump.AddStuckObject(knife.gameObject);
                knifeTransform.localPosition = Random.insideUnitCircle.normalized * _stumpRadius;
                Utils.LookAt2D(knifeTransform, _stump.transform.position);
            }
        }
        
        private void OnLevelCompletionHandler(LevelData levelData)
        {
            _stump.Break();
            DeactivateStump();
            _crackedStump.PlayAnimation();
        }

        private void OnGameOverHandler(GameSessionData gameSessionData)
        {
            DeactivateStump();
        }

        private void ActivateStump()
        {
            _stump.gameObject.SetActive(true);
            _stump.StartSpin();
            _stump.PlayAppearingAnimation();
        }

        private void DeactivateStump()
        {
            _stump.StopSpin();
            _stump.gameObject.SetActive(false);
        }


    }
}