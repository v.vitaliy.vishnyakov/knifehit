﻿namespace _Core.Levels
{
    public class CasualLevelData : LevelData
    {
        public int KnivesGoal { get; }
        public int LevelNumber { get; }

        public CasualLevelData(int knivesGoal, int levelNumber)
        {
            KnivesGoal = knivesGoal;
            LevelNumber = levelNumber;
        }
    }
}