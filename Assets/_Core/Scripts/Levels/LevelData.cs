﻿namespace _Core
{
    public interface LevelData
    {
        public int KnivesGoal { get; }
        public int LevelNumber { get; }
    }
}