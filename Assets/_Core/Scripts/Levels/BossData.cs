﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "BossData", menuName = "Boss Data", order = 0)]
    public class BossData : ScriptableObject
    {
        public Sprite skin;
        public KnifeShopItemModel rewardKnife;
        public StumpConfiguration stumpConfiguration;
        public BossRarity bossRarity;
    }
}