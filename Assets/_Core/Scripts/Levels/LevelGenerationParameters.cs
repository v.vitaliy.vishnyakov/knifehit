﻿using UnityEngine;

namespace _Core.Scripts.Levels
{
    [CreateAssetMenu(fileName = "LevelGenerationParameters", menuName = "Parameters/Level Generation Parameters", order = 0)]
    public class LevelGenerationParameters : ScriptableObject
    {
        public float difficultyDivider;
        [Header("Casual level generation")]
        public int maxKnivesOnStump;
        public int minGoal;
        public int maxGoal;
        [Range(0f, 1f)] public float appleSpawnChance;

        [Header("Default Stump configurations")]
        public ConstStumpConfiguration defaultConstStumpConfiguration;
        public DashStumpConfiguration defaultDashStumpConfiguration;
        public RandomStumpConfiguration defaultRandomStumpConfiguration;

        [Header("Boss level generation")]
        public int bossFrequency;
        [Range(0f, 1f)] public float rareBossChance;
        
        
    }
}