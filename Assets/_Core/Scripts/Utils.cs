using UnityEngine;

public static class Utils
{
    public static float GetRandomSign()
    {
        return Random.value <= 0.5 ? 1 : -1;
    }
    
    public static void LookAt2D(Transform transformToRotate, Vector2 pointToLookAt)
    {
        float offset = 90F;
        Vector2 direction = pointToLookAt - (Vector2)transformToRotate.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transformToRotate.rotation = Quaternion.Euler(0,0, angle + offset);
    }
}