﻿using System;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Core
{
    public class Knife : MonoBehaviour, IPoolableObject, IFallingObject
    {
        public event Action OnStumpHit;
        public event Action OnKnifeHit;
        public event Action OnAppleHit;

        [Header("Parameters")]
        [SerializeField] private float _lengthOnStuck;
        [SerializeField] private LayerMask _stumpLayer;
        [Header("Animation parameters")]
        [SerializeField] private float _spawnDuration;
        [SerializeField] private float _spawnFloatingDistance;
        [SerializeField] private float _fallDistance;
        [SerializeField] private float _fallDuration;
        [Header("Components references")] 
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private Transform _knifeBottom;
        [SerializeField] private string _throwSoundKey;
        
        private float _onThrowSpeed;
        
        public bool IsThrown { get; private set; }
        public bool IsStumped { get; private set; }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (IsThrown)
            {
                if (other.CompareTag(Tags.Stump))
                {
                    HandleStumpHit(other.transform);
                    return;
                }

                if (other.CompareTag(Tags.Knife))
                {
                    HandleKnifeHit();
                    return;
                }

                if (other.CompareTag(Tags.Apple))
                {
                    HandleAppleHit();
                }
            }
        }
        
        private void Start()
        {
            Initialize();
        }

        public void Initialize()
        {
            IsThrown = false;
            IsStumped = false;
        }

        private void Update()
        {
            if (IsThrown)
            {
                transform.position += Vector3.up * _onThrowSpeed * Time.deltaTime;
            }
        }
        
        public void Throw(float throwSpeed)
        {
            if (IsThrown || IsStumped) return;
            IsThrown = true;
            _onThrowSpeed = throwSpeed;
            SoundManager.PlaySFX(_throwSoundKey);
        }
        
        private void HandleStumpHit(Transform stumpTransform)
        {
            transform.Translate(0, _lengthOnStuck-GetDistanceToStump(), 0);
            IsThrown = false;
            IsStumped = true;
            transform.SetParent(stumpTransform);
            stumpTransform.GetComponent<Stump>().AddStuckObject(gameObject);
            OnStumpHit?.Invoke();
        }

        private void HandleKnifeHit()
        {
            IsThrown = false;
            OnKnifeHit?.Invoke();
            Disable();
        }

        private void HandleAppleHit()
        {
            OnAppleHit?.Invoke();
        }

        private float GetDistanceToStump()
        {
            RaycastHit2D hit = Physics2D.Raycast(_knifeBottom.position, -transform.up, 5, _stumpLayer);
            return hit.distance;
        }
        
        public void PlaySpawnAnimation()
        {
            Color initialColor = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, 0);
            _spriteRenderer.DOColor(initialColor, _spawnDuration).From();
            transform.DOMove(new Vector2(transform.position.x, transform.position.y-_spawnFloatingDistance), _spawnDuration).From();
        }
        
        public void SetSkin(Sprite knifeSkin)
        {
            _spriteRenderer.sprite = knifeSkin;
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            transform.parent = null;
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeSelf;
        }

        public void StartFalling()
        {
            transform.DOMove(new Vector2(transform.position.x, transform.position.y - _fallDistance), _fallDuration).SetEase(Ease.Linear).OnComplete(Disable);
            transform.DORotate(new Vector3(0, 0, transform.rotation.eulerAngles.z + Random.Range(45f, 90f)), _fallDuration);
        }

        public void ClearParent()
        {
            transform.parent = null;
        }
    }
}