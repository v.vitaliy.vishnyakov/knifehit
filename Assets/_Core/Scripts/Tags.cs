﻿using UnityEngine;

namespace _Core
{
    public static class Tags
    {
        public const string Stump = "Stump";
        public const string Knife = "Knife";
        public const string Apple = "Apple";
    }
}