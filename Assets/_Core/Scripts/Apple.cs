﻿using DG.Tweening;
using UnityEngine;

namespace _Core
{
    public class Apple : MonoBehaviour, IPoolableObject, IFallingObject
    {
        [Header("Animation parameters")]
        [SerializeField] private float _fallDistance;
        [SerializeField] private float _fallDuration;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            HandleHit();
        }

        private void HandleHit()
        {
            Disable();
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            transform.parent = null;
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        public void StartFalling()
        {
            transform.DOMove(new Vector2(transform.position.x, transform.position.y - _fallDistance), _fallDuration).SetEase(Ease.Linear).OnComplete(Disable);
            transform.DORotate(new Vector3(0, 0, transform.rotation.eulerAngles.z + Random.Range(45f, 90f)), _fallDuration);
        }

        public void ClearParent()
        {
            transform.parent = null;
        }
    }
}