﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class MenuScreen : Screen
    {
        [SerializeField] private Image _knifeImage;
        [SerializeField] private MyButton _playButton;
        [SerializeField] private MyButton _knivesButton;
        [SerializeField] private TextMeshProUGUI _applesCountLabel;
        [SerializeField] private TextMeshProUGUI _highScoreLabel;

        private void Awake()
        {
            _playButton.SubscribeOnClick(OnPlayButtonClick);
            _knivesButton.SubscribeOnClick(OnKnivesButtonClick);
        }

        public override void Show(IScreenParameters screenParameters)
        {
            if (screenParameters is MenuScreenParameters menuScreenParameters)
            {
                _applesCountLabel.text = menuScreenParameters.applesCount.ToString();
                _highScoreLabel.text = menuScreenParameters.highScore.ToString();
                _knifeImage.sprite = menuScreenParameters.knifeSkin;
            }
            Show();
        }

        private void OnPlayButtonClick()
        {
            GameManager.StartGame();
        }

        private void OnKnivesButtonClick()
        {
            UIManager.ShowScreen<ShopScreen>();
            Hide();
        }
    }
}