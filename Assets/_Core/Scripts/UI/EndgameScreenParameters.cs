﻿namespace _Core
{
    public class EndgameScreenParameters: IScreenParameters
    {
        public int finalScore;
        public int applesCount;
    }
}