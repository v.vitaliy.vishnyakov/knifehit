﻿using System;
using System.Collections.Generic;
using _Core.Scripts.Levels;
using UnityEngine;

namespace _Core
{
    public class UIManager : Singleton<UIManager>
    {
        [SerializeField] private List<Screen> _screens;
        [SerializeField] private HUD _hud;
        
        protected override void Initialize() { }

        private void Start()
        {
            GameEventsModel gameEventsModel = new GameEventsModel
            {
                onGameStart = OnGameStartHandler,
                onLevelStartUp = OnLevelStartUpHandler,
                onKnifeAppleHit = OnAppleHitHandler,
                onKnifeStumpHit = OnKnifeStumpHitHandler,
                onLevelCompletion = OnLevelCompletionHandler,
                onGameOver = OnGameOverHandler
            };
            GameManager.SubscribeToGameEvents(gameEventsModel);
        }
        
        private static void ShowHUD()
        {
            ClearHUD();
            _instance._hud.Show();
        }
        
        private static void ClearHUD()
        {
            _instance._hud.SetApplesCount(0);
            _instance._hud.SetScore(0);
        }
        
        public static void HideHUD()
        {
            _instance._hud.Hide();
        }

        private void OnGameStartHandler()
        {
            HideScreen<MenuScreen>();
            ShowHUD();
        }

        private void OnLevelStartUpHandler(LevelData levelData)
        {
            if (levelData.LevelNumber == 1)
            {
                _hud.Initialize(levelData.KnivesGoal);
            }
            else
            {
                _hud.ResetKnivesCounter(levelData.KnivesGoal);
            }
            if (levelData is BossLevelData bossLevelData)
            {
                _hud.ShowBossLabel(bossLevelData.BossData.bossRarity);
            }
            _hud.SetLevelCounter(levelData.LevelNumber);
        }

        private void OnKnifeStumpHitHandler(int score)
        {
            _hud.AddKnifeToCounter();                 
            _hud.SetScore(score);    
        }

        private void OnAppleHitHandler(int applesCount)
        {
            _hud.SetApplesCount(applesCount);
        }

        private void OnLevelCompletionHandler(LevelData levelData)
        {
            if (levelData is BossLevelData bossLevelData)
            {
            }
        }

        private void OnGameOverHandler(GameSessionData gameSessionData)
        {
            _hud.Hide();
            EndgameScreenParameters endgameScreenParameters = new EndgameScreenParameters
            {
                applesCount = gameSessionData.apples,
                finalScore = gameSessionData.score
            };
            ShowScreen<EndgameScreen>(endgameScreenParameters);
        }

        public static void PlayKnifeWinningAnimation(Sprite knifeSkin)
        {
            _instance._hud.ShowKnifeRewardLabel(knifeSkin);
        }
        

        public static void ShowScreen<T>() where T: Screen
        {
            Screen screen = GetScreen<T>();
            if (screen is MenuScreen)
            {
                MenuScreenParameters menuScreenParameters = new MenuScreenParameters
                {
                    applesCount = UserManager.UserData.totalApples,
                    highScore = UserManager.UserData.highScore,
                    knifeSkin = ItemsManager.CurrentKnifeSkin
                };
                screen.Show(menuScreenParameters);
            }

            screen.Show();
        }
        
        public static void ShowScreen<T>(IScreenParameters screenParameters) where T: Screen
        {
            Screen screen = GetScreen<T>();
            if (screen is MenuScreen)
            {
                MenuScreenParameters menuScreenParameters = new MenuScreenParameters
                {
                    applesCount = UserManager.UserData.totalApples,
                    highScore = UserManager.UserData.highScore,
                    knifeSkin = ItemsManager.CurrentKnifeSkin
                };
                screen.Show(menuScreenParameters);
            }
            screen.Show(screenParameters);
        }

        public static void HideScreen<T>() where T: Screen
        {
            Screen screen =  GetScreen<T>();
            screen.Hide();
        }
        
        public static T GetScreen<T>() where T : Screen
        {
            foreach (Screen screen in _instance._screens)
            {
                if (screen is T)
                {
                    return screen as T;
                }
            }
            return null;
        }

        public static void HideAllScreens()
        {
            foreach (Screen screen in _instance._screens)
            {
                if (screen.IsShowing)
                {
                    screen.Hide();
                }
            }
        }
    }
}