﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class KnifeRewardLabel : MonoBehaviour
    {
        [SerializeField] private Image _knifeImage;
        [SerializeField] private Image _backgroundImage;

        [Header("Animation parameters")] 
        [SerializeField] private float _appearingDuration;
        [SerializeField] private float _floatingDuration;

        private Sequence _animation;
        
        private void Awake()
        {
            InitializeAnimation();
        }

        private void InitializeAnimation()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), _appearingDuration).From());
            sequence.Append(_backgroundImage.transform.DORotate(new Vector3(0, 0, 50f + transform.rotation.eulerAngles.z), _floatingDuration));
            sequence.Append(transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), _appearingDuration));
            sequence.Pause();
            sequence.onComplete += () =>
            {
                sequence.Rewind();
                gameObject.SetActive(false);
            };
            _animation = sequence;
        }

        public void SetKnifeImage(Sprite knifeSprite)
        {
            _knifeImage.sprite = knifeSprite;
        }

        public void PlayAnimation()
        {
            gameObject.SetActive(true);
            _animation.Play();
        }
    }
}