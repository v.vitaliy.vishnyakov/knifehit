﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace _Core
{
    public class BossLabelBehaviour : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private AnimationCurve _appearing;
        [SerializeField] private AnimationCurve _dissapearing;

        [SerializeField] private float _appearingDuration;
        [SerializeField] private float _disappearingDuration;
        
        private Vector2 _initialPos;
        private Vector2 startPos;
        private Vector2 endPos;

        private void Awake()
        {
            _text.text = string.Empty;
            _initialPos = transform.position;
            startPos = new Vector3(_initialPos.x - UnityEngine.Screen.width * 0.5f - _text.rectTransform.rect.width * 0.5f,
                _initialPos.y);
            endPos = new Vector3(_initialPos.x + UnityEngine.Screen.width * 0.5f + _text.rectTransform.rect.width * 0.5f,
                _initialPos.y);
            transform.position = startPos;
        }

        public void InitializeAndPlay(string text)
        {
            SetText(text);
            PlayAnimation();
        }

        private void PlayAnimation()
        {
            transform.position = startPos;
            Sequence sequence = DOTween.Sequence();
            sequence.Append(transform.DOMove(_initialPos, _appearingDuration).SetEase(_appearing));
            sequence.Append(transform.DOMove(endPos, _disappearingDuration).SetEase(_dissapearing));
            sequence.Play();
        }

        private void SetText(string text)
        {
            _text.text = text;
        }
    }
}