﻿using UnityEngine;

namespace _Core
{
    public abstract class Screen : MonoBehaviour
    {
        public bool IsShowing => gameObject.activeSelf;
        
        public virtual void Show()
        {
            gameObject.SetActive(true);
        }
        
        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }

        public abstract void Show(IScreenParameters screenParameters);
    }
}