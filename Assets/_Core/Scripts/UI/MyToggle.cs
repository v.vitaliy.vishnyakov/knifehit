﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Core
{
    public class MyToggle : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private Toggle _toggle;

        private void Awake()
        {
            _toggle.onValueChanged.AddListener(OnValueChanged);
        }

        public void Initialize(bool initialValue, UnityAction<bool> onValueChanged)
        {
            _toggle.SetIsOnWithoutNotify(initialValue);
            _toggle.onValueChanged.AddListener(onValueChanged);
        }

        public void SetToggleGroup(ToggleGroup toggleGroup)
        {
            _toggle.group = toggleGroup;
        }

        private void OnValueChanged(bool value)
        {
            
        }
    }
}