﻿using System;
using TMPro;
using UnityEngine;

namespace _Core
{
    public class EndgameScreen : Screen
    {
        [SerializeField] private TextMeshProUGUI _finalScoreLabel;
        [SerializeField] private TextMeshProUGUI _applesLabel;
        [SerializeField] private MyButton _restartButton;
        [SerializeField] private MyButton _menuButton;

        private void Awake()
        {
            _restartButton.SubscribeOnClick(OnRestartButtonClick);
            _menuButton.SubscribeOnClick(OnMenuButtonClick);
        }

        public override void Show(IScreenParameters screenParameters)
        {
            if (screenParameters is EndgameScreenParameters endgameScreenParameters)
            {
                _finalScoreLabel.text = endgameScreenParameters.finalScore.ToString();
                _applesLabel.text = endgameScreenParameters.applesCount.ToString();
            }
            Show();
        }

        private void OnRestartButtonClick()
        {
            GameManager.StartGame();
            Hide();
        }

        private void OnMenuButtonClick()
        {
            UIManager.ShowScreen<MenuScreen>();
            Hide();
        }
    }
}