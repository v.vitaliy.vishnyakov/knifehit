﻿using UnityEngine;

namespace _Core
{
    public class MenuScreenParameters : IScreenParameters
    {
        public Sprite knifeSkin;
        public int applesCount;
        public int highScore;
    }
}