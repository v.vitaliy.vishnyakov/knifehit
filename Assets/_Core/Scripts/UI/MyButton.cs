﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Core
{
    public class MyButton: MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private string _onClickSoundKey;
        private void Awake()
        {
            _button.onClick.AddListener(OnClick);
        }

        public void SubscribeOnClick(UnityAction onClickCallback)
        {
            _button.onClick.AddListener(onClickCallback);
        }

        private void OnClick()
        {
            if (_onClickSoundKey != string.Empty)
            {
                SoundManager.PlaySFX(_onClickSoundKey);    
            }
        }
    }
}