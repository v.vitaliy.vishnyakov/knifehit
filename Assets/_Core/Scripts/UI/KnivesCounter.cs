﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class KnivesCounter : MonoBehaviour
    {
        [SerializeField] private Image _knifeImage;
        [SerializeField] private LayoutGroup _verticalLayout;
        
        private int _knivesHit;
        private int _knivesGoal;
        private List<Image> _knivesImages = new List<Image>();

        public void Initialize(int maxKnivesNumber)
        {
            if (_knivesHit !=0)
            {
                Clear();
            }
            _knivesHit = 0;
            _knivesGoal = maxKnivesNumber;
            for (int i = 0; i < maxKnivesNumber; i++)
            {
                Image knifeImage = Instantiate(_knifeImage, _verticalLayout.transform, true);
                _knivesImages.Add(knifeImage);
            }
        }

        public void Clear()
        {
            foreach (Image knivesImage in _knivesImages)
            {
                Destroy(knivesImage.gameObject);
            }
            _knivesImages.Clear();
        }

        public void AddKnifeHit()
        {
            if (_knivesHit >= _knivesGoal) return;
            _knivesImages[_knivesHit].GetComponent<Image>().color = Color.white;
            _knivesHit++;
        }
    }
}