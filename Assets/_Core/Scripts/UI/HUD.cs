﻿using TMPro;
using UnityEngine;

namespace _Core
{
    public class HUD : Screen
    {
        [SerializeField] private TextMeshProUGUI _scoreLabel;
        [SerializeField] private TextMeshProUGUI _applesCountLabel; 
        [SerializeField] private KnivesCounter _knivesCounter;
        [SerializeField] private TextMeshProUGUI _levelCounter;
        [SerializeField] private BossLabelBehaviour _bossLabelBehaviour;
        [SerializeField] private KnifeRewardLabel _knifeRewardLabel;

        public void SetScore(int score)
        {
            _scoreLabel.text = score.ToString();
        }
        
        public void SetApplesCount(int applesCount)
        {
            _applesCountLabel.text = applesCount.ToString();
        }

        public void SetLevelCounter(int value)
        {
            _levelCounter.text = value.ToString();
        }

        public void Reset()
        {
            _scoreLabel.text = "0";
            _applesCountLabel.text = "0";
            _knivesCounter.Clear();
        }

        public void Initialize(int knivesCounterMaxValue)
        {
            Reset();
            _knivesCounter.Initialize(knivesCounterMaxValue);
        }

        public void ResetKnivesCounter(int knivesCounterMaxValue)
        {
            _knivesCounter.Initialize(knivesCounterMaxValue);
        }

        public void AddKnifeToCounter()
        {
            _knivesCounter.AddKnifeHit();
        }

        public override void Show(IScreenParameters screenParameters)
        {
            throw new System.NotImplementedException();
        }

        public void ShowBossLabel(BossRarity bossRarity)
        {
            _bossLabelBehaviour.InitializeAndPlay(bossRarity + " boss!");
        }

        public void ShowKnifeRewardLabel(Sprite knifeSkin)
        {
            _knifeRewardLabel.SetKnifeImage(knifeSkin);
            _knifeRewardLabel.PlayAnimation();
        }
    }
}