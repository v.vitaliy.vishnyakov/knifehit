﻿namespace _Core
{
    public class UserManager : Singleton<UserManager>
    {
        private const string UserDataKey = "User";
        private UserData _userData;

        public static UserData UserData => _instance._userData;

        protected override void Initialize()
        {
            GameEventsModel gameEventsModel = new GameEventsModel
            {
                onGameOver = OnGameOverHandler
            };
            GameManager.SubscribeToGameEvents(gameEventsModel);
        }

        private void Start()
        {
            LoadUserData();
        }

        public static bool TrySpendApples(int apples)
        {
            if (_instance._userData.totalApples >= apples)
            {
                _instance._userData.totalApples -= apples;
                return true;
            }
            return false;
        }

        public static void LoadUserData()
        {
            _instance._userData = SaveManager.Load<UserData>(UserDataKey) ?? new UserData
            {
                totalApples = 0,
                highScore = 0
            };
        }

        private void OnGameOverHandler(GameSessionData gameSessionData)
        {
            CheckForNewHighScore(gameSessionData.score);
            _userData.totalApples += gameSessionData.apples;
            SaveUserData();
        }

        private void CheckForNewHighScore(int score)
        {
            if (_userData.highScore < score)
            {
                _userData.highScore = score;
            }
        }

        private void SaveUserData()
        {
            SaveManager.Save(UserDataKey, _userData);
        }
    }
}