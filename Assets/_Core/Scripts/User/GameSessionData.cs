﻿using System;

namespace _Core
{
    [Serializable]
    public class GameSessionData
    {
        public int score;
        public int apples;
        public int levelCounter;
        public int knifeStumpHits;
    }
}