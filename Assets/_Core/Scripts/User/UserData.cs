﻿using System;

namespace _Core
{
    [Serializable]
    public class UserData
    {
        public int highScore;
        public int totalApples;
    }
}