﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core
{
    public class SpawnManager : Singleton<SpawnManager>
    {
        private delegate bool Condition<in T>(T obj);
        [SerializeField] private SpawnPrefabs _prefabs;

        private List<Knife> _knivesPool = new List<Knife>();
        private List<Apple> _applesPool = new List<Apple>();

        protected override void Initialize() { }

        public static Knife SpawnKnife(Transform transform = null)
        {
            Knife knife = _instance.Spawn(_instance._knivesPool, _instance._prefabs.knife);
            knife.gameObject.transform.SetParent(transform);
            knife.Initialize();
            knife.SetSkin(ItemsManager.CurrentKnifeSkin);
            return knife;
        }

        public static Apple SpawnApple()
        {
            return _instance.Spawn(_instance._applesPool, _instance._prefabs.apple);
        }

        private T Spawn<T, U>(List<T> pool, T prefab, Condition<T> condition = null) 
            where T: MonoBehaviour, IPoolableObject
            where U: T
        {
            T obj = null;
            foreach (var poolObj in pool)
            {
                if (poolObj is U && !poolObj.gameObject.activeInHierarchy)
                {
                    if (condition != null)
                    {
                        if (!condition(poolObj))
                        {
                            continue;
                        }
                    }
                    obj = poolObj;
                    break;
                }
            }
            if (obj == null)
            {
                obj = Instantiate(prefab);
                pool.Add(obj);
            }
            obj.Enable();
            return obj;       
        }
        private T Spawn<T>(List<T> pool, T prefab, Condition<T> condition = null) where T : MonoBehaviour, IPoolableObject
        {
            return Spawn<T, T>(pool, prefab, condition);
        }

        public static void DisableAllKnives()
        {
            DisableAllObjectsInPool(_instance._knivesPool);
        }

        public static void DisableAllApples()
        {
            DisableAllObjectsInPool(_instance._applesPool);
        }
        
        private static void DisableAllObjectsInPool<T>(List<T> pool) where T: IPoolableObject
        {
            foreach (var obj in pool)
            {
                if (obj.IsActive())
                {
                    obj.Disable();
                }
            }
        }
    }
}