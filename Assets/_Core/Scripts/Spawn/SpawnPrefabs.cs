﻿using System;
using System.Collections.Generic;

namespace _Core
{
    [Serializable]
    public class SpawnPrefabs
    {
        public Knife knife;
        public Apple apple;
    }
}