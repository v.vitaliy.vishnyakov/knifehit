﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "GameParameters", menuName = "Parameters/Game Parameters", order = 0)]
    public class GameParameters : ScriptableObject
    {
        public float _delayBetweenLevels;
        [Header("Knife")]
        public float knifeThrowDelay;
        public float knifeThrowSpeed;
        [Header("Balance")]
        [Range(0f, 1f)] public float appleSpawnChance;
        public int halfsForApple;
        public float appleSpawnOffset;
    }
}