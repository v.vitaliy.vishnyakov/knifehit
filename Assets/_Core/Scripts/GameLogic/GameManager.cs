﻿using System;
using System.Collections;
using _Core.Scripts.Levels;
using UnityEngine;

namespace _Core
{
    public class GameManager : Singleton<GameManager>
    {
        private event Action OnGameStart;
        private event Action<int> OnKnifeStumpHit;
        private event Action<int> OnKnifeAppleHit;
        private event Action<LevelData> OnLevelStartUp;
        private event Action<LevelData> OnLevelCompletion;
        private event Action<GameSessionData> OnGameOver;
        
        [Header("Parameters")]
        [SerializeField] private GameParameters _parameters;
        [Header("Objects references")]
        [SerializeField] private Transform _knifeSpawnSpot;
        [SerializeField] private ParticleSystem _hitKnifeEffect;
        
        private bool _isPlaying;
        private float _timer;
        private Knife _activeKnife;
        private LevelData _levelData;
        private GameSessionData _gameSessionData;

        protected override void Initialize()
        {
            Vibration.Init();
        }
        
        private void Start()
        {
#if UNITY_EDITOR
            UIManager.HideAllScreens();
#endif
            InitializeOnStartup();
        }

        private void InitializeOnStartup()
        {
            LoadAllData();            
            UIManager.ShowScreen<MenuScreen>();
        }

        private void LoadAllData()
        {
            UserManager.LoadUserData();
            ItemsManager.LoadAllData();
        }
        
        private void Update()
        {
            HandleControls();
        }
        
        private void HandleControls()
        {
            _timer += Time.deltaTime;
#if UNITY_EDITOR_64
            if(Input.GetKeyDown(KeyCode.K) && _isPlaying)
#else
            if (Input.GetTouch(0).phase == TouchPhase.Began && _isPlaying)
#endif
            {

                if (_timer >= _parameters.knifeThrowDelay)
                {
                    _activeKnife.Throw(_parameters.knifeThrowSpeed);
                    _timer = 0;
                }
            }
        }

        public static void StartGame()
        {
            _instance._isPlaying = true;
            _instance._gameSessionData = new GameSessionData();
            _instance.SetupNextLevel();
            _instance.OnGameStart?.Invoke();
        }
        
        private void SetupNextLevel()
        {
            _gameSessionData.levelCounter++;
            _gameSessionData.knifeStumpHits = 0;
            _levelData = LevelManager.SetupLevel(_gameSessionData.levelCounter);
            OnLevelStartUp?.Invoke(_levelData);
            SpawnNewActiveKnife();
        }
        
        private void SpawnNewActiveKnife()
        {
            _activeKnife = SpawnManager.SpawnKnife();
            _activeKnife.transform.position = _knifeSpawnSpot.position;
            _activeKnife.transform.rotation = Quaternion.Euler(0,0,180);
            _activeKnife.OnStumpHit += OnKnifeStumpHitHandler;
            _activeKnife.OnKnifeHit += OnKnifeHitKnifeHandler;
            _activeKnife.OnAppleHit += OnAppleHitHandler;
            _activeKnife.PlaySpawnAnimation();
        }

        private void OnKnifeStumpHitHandler()
        {
            Vibration.VibratePop();
            _gameSessionData.knifeStumpHits++;
            _gameSessionData.score++;
            _activeKnife.OnStumpHit -= OnKnifeStumpHitHandler;
            _activeKnife.OnKnifeHit -= OnKnifeHitKnifeHandler;
            _activeKnife.OnAppleHit -= OnAppleHitHandler;
            OnKnifeStumpHit?.Invoke(_gameSessionData.score);
            if (_gameSessionData.knifeStumpHits == _levelData.KnivesGoal)
            {
                CompleteLevel();
            }
            else
            {
                SpawnNewActiveKnife();
            }
        }

        private void CompleteLevel()
        {
            if (_levelData is BossLevelData bossLevelData)
            {
                if (ItemsManager.TryUnlockItem(bossLevelData.BossData.rewardKnife.Id))
                {
                    Debug.Log("Knife unlocked");
                    UIManager.PlayKnifeWinningAnimation(bossLevelData.BossData.rewardKnife.Image);
                }
            }
            OnLevelCompletion?.Invoke(_levelData);
            StartCoroutine(WaitForSeconds(_parameters._delayBetweenLevels));
        }

        private IEnumerator WaitForSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            SetupNextLevel();
        }

        private void OnKnifeHitKnifeHandler()
        {
            Vibration.VibratePop();
            PlayKnifeHitEffect();
            _activeKnife.OnStumpHit -= OnKnifeStumpHitHandler;
            _activeKnife.OnKnifeHit -= OnKnifeHitKnifeHandler;
            _activeKnife.OnAppleHit -= OnAppleHitHandler;
            EndGame();
        }

        private void PlayKnifeHitEffect()
        {
            _hitKnifeEffect.transform.position = _activeKnife.transform.position;
            _hitKnifeEffect.Play();
        }

        private void EndGame()
        {
            _isPlaying = false;
            SpawnManager.DisableAllKnives();
            SpawnManager.DisableAllApples();
            OnGameOver?.Invoke(_gameSessionData);
        }

        private void OnAppleHitHandler()
        {
            _gameSessionData.apples += _parameters.halfsForApple;
            OnKnifeAppleHit?.Invoke(_gameSessionData.apples);
        }

        public static void SubscribeToGameEvents(GameEventsModel gameEventsModel)
        {
            _instance.OnGameStart += gameEventsModel.onGameStart;
            _instance.OnKnifeStumpHit += gameEventsModel.onKnifeStumpHit;
            _instance.OnKnifeAppleHit += gameEventsModel.onKnifeAppleHit;
            _instance.OnLevelStartUp += gameEventsModel.onLevelStartUp;
            _instance.OnLevelCompletion += gameEventsModel.onLevelCompletion;
            _instance.OnGameOver += gameEventsModel.onGameOver;
        }
    }
}