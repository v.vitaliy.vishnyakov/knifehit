﻿using System;

namespace _Core
{
    public class GameEventsModel
    {
        public Action onGameStart;
        public Action<int> onKnifeStumpHit;
        public Action<int> onKnifeAppleHit;
        public Action<LevelData> onLevelStartUp;
        public Action<LevelData> onLevelCompletion;
        public Action<GameSessionData> onGameOver;
    }
}