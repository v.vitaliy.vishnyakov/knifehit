﻿using System;

namespace _Core
{
    [Serializable]
    public class RandomStumpConfig : StumpConfig
    {
        public float minRotateAngle;
        public float maxRotateAngle;
        public float minRotateTime;
        public float maxRotateTime;
    }
}