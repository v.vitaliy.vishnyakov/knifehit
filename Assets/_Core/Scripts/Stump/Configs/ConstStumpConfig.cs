﻿using System;

namespace _Core
{
    [Serializable]
    public class ConstStumpConfig : StumpConfig
    {
        public float speed;
        public float direction;
    }
}