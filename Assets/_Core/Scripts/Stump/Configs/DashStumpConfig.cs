﻿using System;

namespace _Core
{
    [Serializable]
    public class DashStumpConfig : StumpConfig
    {
        public float angle;
        public float time;
        public float direction;
    }
}