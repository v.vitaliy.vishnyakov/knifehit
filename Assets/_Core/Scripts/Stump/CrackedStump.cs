﻿using UnityEngine;

namespace _Core
{
    public class CrackedStump : MonoBehaviour
    {
        [SerializeField] private GameObject _piecesParent;

        public void PlayAnimation()
        {
            _piecesParent.SetActive(true);
        }
    }
}