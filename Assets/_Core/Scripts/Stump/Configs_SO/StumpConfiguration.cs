﻿using UnityEngine;

namespace _Core
{
    public abstract class StumpConfiguration : ScriptableObject
    {
        public abstract StumpConfig Config { get; }
    }
}