﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "DashStumpConfiguration", menuName = "Stump Configs/Dash Stump Configuration", order = 0)]
    public class DashStumpConfiguration : StumpConfiguration
    {
        [SerializeField] private DashStumpConfig _config;

        public override StumpConfig Config => _config;
    }
}