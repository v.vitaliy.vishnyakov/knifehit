﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "ConstStumpConfiguration", menuName = "Stump Configs/Const Stump Configuration", order = 0)]
    public class ConstStumpConfiguration : StumpConfiguration
    {
        [SerializeField] private ConstStumpConfig _config;

        public override StumpConfig Config => _config;
    }
}