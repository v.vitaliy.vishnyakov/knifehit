﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "RandomStumpConfiguration", menuName = "Stump Configs/Random Stump Configuration", order = 0)]
    public class RandomStumpConfiguration : StumpConfiguration
    {
        [SerializeField] private RandomStumpConfig _config;

        public override StumpConfig Config => _config;
    }
}