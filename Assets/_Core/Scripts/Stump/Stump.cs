﻿using UnityEngine;
using DG.Tweening;
using Random = UnityEngine.Random;

namespace _Core
{
    [RequireComponent(typeof(CircleCollider2D))]
    public class Stump : MonoBehaviour
    {
        [Header("Parameters")]
        [SerializeField] private float _shakeDuration;
        [SerializeField] private Vector3 _shakeStrength;
        [SerializeField] private int _shakeVibrato;
        [SerializeField] private float _shakeRandomness;
        [SerializeField] private bool _shakeSnapping;
        [SerializeField] private bool _shakeFadeout;
        [SerializeField] private Color _hitColor;
        [SerializeField] private float _hitDuration;
        [SerializeField] private string _onKnifeHitSoundKey;
        [SerializeField] private Sprite _defaultSkin;
        [SerializeField] private float _appearingAnimationDuration;
        [Header("Components References")] 
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private ParticleSystem _hitEffect;
        [SerializeField] private Transform _stuckObjectsParent;
        
        private Tweener _tweener;
        private StumpConfig _stumpConfig;
        

        public bool IsSpinning { get; private set; }

        public void Initialize(StumpConfig stumpConfig)
        {
            transform.rotation = Quaternion.identity;
            if (IsSpinning)
            {
                StopSpin();
            }
            
            _stumpConfig = stumpConfig;
            if (stumpConfig is ConstStumpConfig)
            {
                ConfigureConstTween();
            }
            if (stumpConfig is DashStumpConfig)
            {
                ConfigureDashTween();
            }
            if (stumpConfig is RandomStumpConfig)
            {
                ConfigureRandomTween();
            }
        }

        private void ConfigureConstTween()
        {
            ConstStumpConfig constStumpConfig = (ConstStumpConfig) _stumpConfig;
            _tweener = transform.DORotate(new Vector3(0, 0, 360 * Mathf.Sign(constStumpConfig.direction)), constStumpConfig.speed,
                RotateMode.FastBeyond360);
            _tweener.SetEase(Ease.Linear).Pause();
            _tweener.SetLoops(-1);
        }

        private void ConfigureDashTween()
        {
            DashStumpConfig dashStumpConfig = (DashStumpConfig) _stumpConfig;
            _tweener = transform.DORotate(
                new Vector3(0, 0,
                    Mathf.Sign(dashStumpConfig.direction) * dashStumpConfig.angle + transform.rotation.eulerAngles.z), dashStumpConfig.time,
                RotateMode.FastBeyond360).Pause();
            _tweener.OnComplete(() =>
            {
                ConfigureDashTween();
                _tweener.Play();
            });
        }
        
        private void ConfigureRandomTween()
        {
            if (!(_stumpConfig is RandomStumpConfig randomDashStump)) return;
            float angle = Random.Range(randomDashStump.minRotateAngle, randomDashStump.maxRotateAngle) * Utils.GetRandomSign();
            float time = Random.Range(randomDashStump.minRotateTime, randomDashStump.maxRotateTime);
            _tweener = transform.DORotate(new Vector3(0, 0, angle + transform.rotation.eulerAngles.z), time,  RotateMode.FastBeyond360).Pause();
            _tweener.OnComplete(()=>
            {
                ConfigureRandomTween();
                _tweener.Play();
            });
        }

        public void StartSpin()
        {
            if (_tweener == null) return;
            IsSpinning = true;
            _tweener.Play();

        }

        public void StopSpin()
        {
            if (_tweener == null) return;
            IsSpinning = false;
            _tweener.Rewind();
        }

        public void SetDefaultSkin()
        {
            _spriteRenderer.sprite = _defaultSkin;
        }
        
        public void SetSkin(Sprite stumpSkin)
        {
            _spriteRenderer.sprite = stumpSkin;
        }

        public void OnKnifeHitHandler(int score)
        {
            PlayHitEffect();
            SoundManager.PlaySFX(_onKnifeHitSoundKey);
            Color defaultColor = _spriteRenderer.color;
            transform.DOShakePosition(_shakeDuration, _shakeStrength, _shakeVibrato, _shakeRandomness, _shakeSnapping,
                _shakeFadeout);
            _spriteRenderer.DOColor(_hitColor, _hitDuration * 0.5f).OnComplete(() =>
            {
                _spriteRenderer.DOColor(defaultColor, _hitDuration * 0.5f);
            });
        }

        private void PlayHitEffect()
        {
            if (_hitEffect.isPlaying)
            {
                _hitEffect.Clear();
            }
            _hitEffect.Play();
        }

        public void AddStuckObject(GameObject obj)
        {
            obj.transform.SetParent(_stuckObjectsParent, true);
        }

        public void Break()
        {
            IFallingObject[] objs = _stuckObjectsParent.GetComponentsInChildren<IFallingObject>();
            foreach (IFallingObject stuckObject in objs)
            {
                stuckObject.ClearParent();
                stuckObject.StartFalling();
            }
        }
        
        public void PlayAppearingAnimation()
        {
            transform.DOScale(new Vector3(0.2f, 0.2f, 0.2f), _appearingAnimationDuration).From();
        }
    }
}