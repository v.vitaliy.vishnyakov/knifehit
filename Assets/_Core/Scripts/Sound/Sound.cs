﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "Sound", menuName = "Sound", order = 0)]
    public class Sound : ScriptableObject
    {
        [SerializeField] private string _key;
        [SerializeField] private AudioClip _audioClip;
        
        public string Key => _key;
        public AudioClip AudioClip => _audioClip;
    }
}