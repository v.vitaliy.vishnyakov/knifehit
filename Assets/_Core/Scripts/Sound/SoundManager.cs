﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core
{
    public class SoundManager : Singleton<SoundManager>
    {
        [SerializeField] private List<Sound> _sfxSounds;
        [Header("Components References")]
        [SerializeField] private AudioSource _sfxSource;

        protected override void Initialize() { }
        
        public static void PlaySFX(string sfxKey)
        {
            Sound sound = FindSFXSound(sfxKey);
            _instance._sfxSource.PlayOneShot(sound.AudioClip);
        }

        private static Sound FindSFXSound(string key)
        {
            return _instance._sfxSounds.Find(s => s.Key == key);
        }
    }
}