﻿using System;
using System.Collections.Generic;
using _Core.Scripts.Levels;
using UnityEngine;

namespace _Core
{
    public class ItemsManager : Singleton<ItemsManager>
    {
        private event Action<int> OnBossKnifeGet;
        
        private const string ItemsDataKey = "ItemsDataKey";
        private const string ItemsStorageFileName = "ItemsStorage";
        
        [SerializeField] private ShopItemModel _itemByDefaultModel;
        [SerializeField] private ShopItemView _shopItemViewPrefab;
        private ItemsData _itemsData;
        private ItemsStorage _itemsStorage;
        
        public static Sprite CurrentKnifeSkin { get; private set; }

        protected override void Initialize() { }

        private void Start()
        {
            SubscribeToGameEvents();
        }

        public static void LoadAllData()
        {
            _instance.LoadFullItemsData();
            _instance.ApplyLastChosenItem();
            _instance.InitializeShopScreen();
        }

        private void SubscribeToGameEvents()
        {
            GameEventsModel gameEventsModel = new GameEventsModel
            {
                onLevelCompletion = OnLevelCompletion
            };
            GameManager.SubscribeToGameEvents(gameEventsModel);
        }
        
        private void LoadFullItemsData()
        {
            _itemsData = LoadItemsData();
            _itemsStorage = Resources.Load<ItemsStorage>(ItemsStorageFileName);
        }
        private void ApplyLastChosenItem()
        {
            ApplyItem(_itemsData._chosenItemId);
        }
        
        private void InitializeShopScreen()
        {
            List<ShopItemView> shopItemViews = new List<ShopItemView>();
            foreach (ShopItemModel shopItem in _itemsStorage.Items)
            {
                ShopItemView shopItemView = Instantiate(_shopItemViewPrefab);
                bool isItemPurchased = IsItemPurchased(shopItem.Id);
                bool isItemChosen = _itemsData._chosenItemId == shopItem.Id;;
                shopItemView.InitializeByItem(shopItem, isItemPurchased, isItemChosen, OnBuyingItem, OnChoosingItem);
                OnBossKnifeGet += shopItemView.OnBossKnifeGetHandler;
                shopItemViews.Add(shopItemView);
            }
            UIManager.GetScreen<ShopScreen>().InitializeScreen(shopItemViews);
        }
        
        private static void ApplyItem(int itemId)
        {
            ShopItemModel shopItemModel = _instance._itemsStorage.Items.Find(x => x.Id == itemId);
            if (shopItemModel is KnifeShopItemModel {Data: KnifeItemData knifeItemData} characterItemModel)
            {
                CurrentKnifeSkin = knifeItemData.knifeSkin;
            }
            _instance._itemsData._chosenItemId = itemId;
            _instance.SaveItemsData();
        }

        private static bool IsItemPurchased(int itemId)
        {
            return _instance._itemsData._purchasedItemsIds.Contains(itemId);
        }
        
        private void OnChoosingItem(IShopItemArgs itemArgs)
        {
            ApplyItem(itemArgs.Id);
        }

        private bool OnBuyingItem(IShopItemArgs itemArgs)
        {
            if (!UserManager.TrySpendApples(itemArgs.Price)) return false;
            _instance._itemsData._purchasedItemsIds.Add(itemArgs.Id);
            _instance.SaveItemsData();
            return true;
        }
        
        private ItemsData LoadItemsData()
        {
            ItemsData loadedItemsData = SaveManager.Load<ItemsData>(ItemsDataKey) ?? new ItemsData
            {
                _purchasedItemsIds = new List<int>(new int[]{_itemByDefaultModel.Id}),
                _chosenItemId = _itemByDefaultModel.Id
            };
            return loadedItemsData;
        }

        private void SaveItemsData()
        {
            SaveManager.Save(ItemsDataKey, _instance._itemsData);
        }

        public static bool TryUnlockItem(int itemId)
        {
            if (_instance._itemsData._purchasedItemsIds.Contains(itemId)) return false;
            _instance._itemsData._purchasedItemsIds.Add(itemId);
            _instance.OnBossKnifeGet?.Invoke(itemId);
            _instance.SaveItemsData();
            return true;

        }

        private void OnLevelCompletion(LevelData levelData)
        {
            if (levelData is BossLevelData bossLevelData)
            {
                if (TryUnlockItem(bossLevelData.BossData.rewardKnife.Id))
                {
                    OnBossKnifeGet?.Invoke(bossLevelData.BossData.rewardKnife.Id);
                }
            }
        }
    }
}