﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class ShopItemView : MonoBehaviour, IPoolableObject
    {
        private event Action OnItemPurchased;
        public event Action<bool> OnPurchaseAttempt;
        
        [Header("Components references")]        
        [SerializeField] private Image _image;
        [SerializeField] private Image _backgroundImage;
        [SerializeField] private MyButton _buyButton;
        [SerializeField] private MyToggle _chooseToggle;
        [SerializeField] private TextMeshProUGUI _priceLabel;

        [Header("Parameters")]
        [SerializeField] private Color _commonItemColor;
        [SerializeField] private Color _rareItemColor;
        
        private Action<IShopItemArgs> _onChooseCallback;
        private Func<IShopItemArgs, bool> _onBuyCallback;
        private ShopItemArgs _shopItemArgs; 
        
        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            _buyButton.SubscribeOnClick(OnBuyButtonClick);
        }

        public void InitializeByItem(ShopItemModel shopItemModel, bool isPurchased, bool isChosen, Func<IShopItemArgs, bool> onBuyCallback, Action<IShopItemArgs> onChooseCallback)
        {
            _shopItemArgs = new ShopItemArgs(shopItemModel);
            _image.sprite = shopItemModel.Image;
            SetBackgroundImageByRarity(shopItemModel.Rarity);
            _priceLabel.text = shopItemModel.Price.ToString();
            _onChooseCallback = onChooseCallback;
            _onBuyCallback = onBuyCallback;
            MakeChoosable(isPurchased);
            _chooseToggle.Initialize(isChosen, OnChooseToggleValueChanged);
        }

        private void OnBuyButtonClick()
        {
            bool result = _onBuyCallback(_shopItemArgs);
            OnPurchaseAttempt?.Invoke(result);
            if (!result) return;
            MakeChoosable(true);
            OnItemPurchased?.Invoke();
        }

        private void OnChooseToggleValueChanged(bool value)
        {
            if (value)
            {
                _onChooseCallback(_shopItemArgs);    
            }
        }

        public void AddToToggleGroup(ToggleGroup toggleGroup)
        {
            _chooseToggle.SetToggleGroup(toggleGroup);
        }
        
        private void MakeChoosable(bool value)
        {
            _buyButton.gameObject.SetActive(!value);
            _chooseToggle.gameObject.SetActive(value);
        }
        
        public void SetParent(Transform parentTransform)
        {
            transform.SetParent(parentTransform);
        }

        public void SubscribeToOnItemPurchased(Action onItemPurchasedHandler)
        {
            OnItemPurchased += onItemPurchasedHandler;
        }
        
        public void Enable()
        {
            gameObject.SetActive(true);
            
        }
        
        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        private void SetBackgroundImageByRarity(ItemRarity itemRarity)
        {
            if (itemRarity == ItemRarity.Common)
            {
                _backgroundImage.color = _commonItemColor;
                return;
            }
            if (itemRarity == ItemRarity.Rare)
            {
                _backgroundImage.color = _rareItemColor;
                return;
            }
        }

        public void OnBossKnifeGetHandler(int knifeId)
        {
            if (knifeId == _shopItemArgs.Id)
            {
                MakeChoosable(true);
            }
        }
    }
}