﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class ShopScreen : Screen
    {
        [SerializeField] private TextMeshProUGUI _applesAmountLabel;
        [SerializeField] private LayoutGroup _layoutGroup;
        [SerializeField] private ToggleGroup _toggleGroup;
        [SerializeField] private MyButton _closeButton;

        private void Awake()
        {
            _closeButton.SubscribeOnClick(OnCloseButtonClick);
        }

        public void InitializeScreen(List<ShopItemView> shopItemViews)
        {
            foreach (ShopItemView itemView in shopItemViews)
            {
                itemView.OnPurchaseAttempt += OnPurchaseAttempt;
                itemView.SubscribeToOnItemPurchased(OnItemPurchased);
                itemView.SetParent(_layoutGroup.transform);
                itemView.AddToToggleGroup(_toggleGroup);
            }
        }

        private void OnCloseButtonClick()
        {
            Hide();
            UIManager.ShowScreen<MenuScreen>();
        }

        private void OnEnable()
        {
            UpdateApplesLabel();
        }

        private void OnItemPurchased()
        {
            UpdateApplesLabel();
        }
        
        [Header("Apple counter shake animation parameters")]
        [SerializeField] private float duration;
        [SerializeField] private float strength;
        [SerializeField] private int vibrato;
        [SerializeField] private float randomness;
        [SerializeField] private bool fadeout;
        
        private void OnPurchaseAttempt(bool isSuccessful)
        {
            if (!isSuccessful)
            {
                _applesAmountLabel.transform.DOShakePosition(duration, strength, vibrato, randomness, false, fadeout);
            }
        }

        private void UpdateApplesLabel()
        {
            _applesAmountLabel.text = UserManager.UserData.totalApples.ToString();
        }

        public override void Show(IScreenParameters screenParameters)
        {
            throw new System.NotImplementedException();
        }
    }
}