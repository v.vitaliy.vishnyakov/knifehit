﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "KnifeShopItemModel", menuName = "Shop/Knife Shop Item Model", order = 0)]
    public class KnifeShopItemModel : ShopItemModel
    {
        [SerializeField] private int _id;
        [SerializeField] private int _price;
        [SerializeField] private Sprite _image;
        [SerializeField] private ItemRarity _rarity; 
        [SerializeField] private KnifeItemData knifeItemData;

        public override int Id => _id;
        public override int Price => _price;
        public override Sprite Image => _image;
        public override ItemRarity Rarity => _rarity;
        public override IShopItemData Data => knifeItemData;
        
    }
}