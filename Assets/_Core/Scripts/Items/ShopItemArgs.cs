﻿namespace _Core
{
    public class ShopItemArgs : IShopItemArgs
    {
        public int Id { get; }
        public int Price { get; }
        
        public ShopItemArgs(ShopItemModel shipShopItemModel)
        {
            Id = shipShopItemModel.Id;
            Price = shipShopItemModel.Price;
        }
    }
}