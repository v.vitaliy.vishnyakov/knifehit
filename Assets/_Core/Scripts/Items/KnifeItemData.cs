﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "KnifeItemData", menuName = "Shop/Knife Item Data", order = 0)]
    public class KnifeItemData : ScriptableObject, IShopItemData
    {
        public Sprite knifeSkin;
    }
}